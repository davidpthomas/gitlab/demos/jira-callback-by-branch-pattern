# Custom callback to Atlassian JIRA based on commits to a Feature branch matching a specific pattern

This demo flow showcases:
- how flexible GitLab CI is for custom requirements
- how to use regex in workflow rules to specifically match complex conditions
- how to callback to JIRA with custom data

## Customer Use Case
In this real Customer Use Case (regulated industry), they follow a standard pattern of creating feature branches with the JIRA KEY/ID in the name, _e.g. KEY-1234-some-description_.  When developers commit to those branches in GitLab, they want to make custom updates in Atlassian JIRA.  This allows them to customize the data in JIRA to match their corporate workflow standards.

In addition to the match, they use this pipeline definition in GitLab project templates to help Developers follow standards without having to think :)

## HOWTO
Instructions are in the .gitlab-ci.yml

## Implementation Note
In this example, the Developer can set a custom JIRA project key as a variable and reference it in the workflow rules of a job.  In the job, they can custom write an [API call back to their JIRA server](https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/).

The purpose of this approach is we currently do not support variable substitution within the regex rules.  Moreover, the custom variable must be on the LHS.  For example,
- **Valid**, :if '$MY_JIRA_KEY =~ $CI_COMMIT_BRANCH'
- **Invalid**, :if '$CI_COMMIT_BRANCH =~ $MY_JIRA_KEY'
- **Invalid**, :if '$CI_COMMIT_BRANCH =~ /^$MY_JIRA_KEY/'


## GitLab Notes
- This provides more flexibility in communicating with JIRA versus our [current integrations](https://docs.gitlab.com/ee/user/project/integrations/jira.html) that requires developers to provide the JIRA ID or Smart Commit directly in the commit comment.
- This is different than setting a branch regex pattern in the global project 'push rules' settings.  In this case, they want users to spin up new projects from a project template and get started coding without having to go manage project settings especially as they may not have permissions.
